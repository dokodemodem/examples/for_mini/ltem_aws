#pragma once
#include <Arduino.h>

static const char AWS_PRIVATE_KEY[] = 
R"(-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAv00gE45qabZGXDRZsPwmqtdDrym7lHYqUSnsDXR7QGesIW8V
//AWS IoT Coreで登録した時に作られるキーを設定してください
2eTlg7staVXEHBDvBilfMDDHw1dMAa3l0x6ySWqpL9WJgqiidGoHDg==
-----END RSA PRIVATE KEY-----)";

static const char AWS_CERT_CRT[] = 
R"(-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAJC+beqmcuzFXT70fQxgekgdObTxMA0GCSqGSIb3DQEB
//AWS IoT Coreで登録した時に作られるキーを設定してください
nZsR+YKchAqq4sw4tvUPTWfBYzstXaKavOk83DDZflfK0AJQc/5FJbvy6GHNbw==
-----END CERTIFICATE-----)";

static const char AWS_ROOT_CA[] = 
R"(-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
//AWS IoT Coreで登録した時に作られるキーを設定してください
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----)";