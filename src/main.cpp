/*
 * Sample program for DokodeModem
 * inside MQTT pub/sub sample for AWS IoT Core.
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
#include <nRF9160Modem.h>
#include <ArduinoJson.h>
#include "modem_setting.h"
#include "secret.h"

DOKODEMO Dm = DOKODEMO();
nRF9160Modem modem = nRF9160Modem();

#define DEVICE_ID "DKM0001" // デバイス名
#define PUBLISH_TOPIC "dokodemo/v1/pub"
#define SUBSCRIBE_TOPIC "dokodemo/v1/sub"

#define SEND_CYCLE_MS 10000
#define IOT_ENDPOINT "*****.iot.ap-northeast-1.amazonaws.com"
#define IOT_PORT 8883

bool modem_init(bool enableSSL)
{
  SerialDebug.println("\r\n### initialize LTE-M");

  Dm.ModemPowerCtrl(ON); // モデムの電源を入れます

  if (!modem.init(UartModem)) // その直後に初期化してください
  {
    SerialDebug.println("### init Error");
    return false;
  }

  // AWS IoT Coreに登録した時の各種暗号鍵を使用します
  if (enableSSL)
  {
    bool ret = modem.setCACert(AWS_ROOT_CA);
    if (!ret)
    {
      SerialDebug.println("err root_ca");
      return false;
    }

    ret = modem.setCertificate(AWS_CERT_CRT);
    if (!ret)
    {
      SerialDebug.println("err certificate");
      return false;
    }

    ret = modem.setPrivateKey(AWS_PRIVATE_KEY);
    if (!ret)
    {
      SerialDebug.println("err private_key");
      return false;
    }
  }

  // 携帯網につなげます
  SerialDebug.println("### Start activate...");
  if (!modem.activate(APN, USERNAME, PASSWORD))
  {
    SerialDebug.println("### Error");
    return false;
  }
  SerialDebug.println("### OK");

  // 機器のIPアドレスを表示します
  std::string ipadd;
  modem.getIPaddress(&ipadd);
  SerialDebug.print("IPaddress: ");
  SerialDebug.println(ipadd.c_str());

  // 機器のUUIDアドレスを表示します
  std::string add;
  modem.getUUID(&add);
  SerialDebug.print("UUID: ");
  SerialDebug.println(add.c_str());

  // 機器のファームウェアバージョンを表示します
  modem.getVersion(&add);
  SerialDebug.print("Version: ");
  SerialDebug.println(add.c_str());

  // LTE-Mの時間をRTCに設定します
  std::tm res = {};
  if (modem.getlocaltime(&res))
  {
    DateTime tm = DateTime(res.tm_year, res.tm_mon + 1, res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec);
    Dm.rtcAdjust(tm);
  }

  // MQTT Brokerに接続します
  SerialDebug.println("### connecting to MQTT broker");
  if (!modem.mqttOpen(IOT_ENDPOINT, IOT_PORT, DEVICE_ID))
  {
    return false;
  }

  // MQTT Brokerにsubscribeします
  if (!modem.mqttSubscribe(SUBSCRIBE_TOPIC))
  {
    SerialDebug.println("subscribe failed...");
    return false;
  }
  else
  {
    SerialDebug.println("subscribed!!");
  }

  SerialDebug.println("### OK");
  return true;
}

uint32_t _nextTimeout;
uint32_t _count = 0;

void setup()
{
  delay(2000); // デバッグ出力のため、USBが認識するまで待ちます。

  Dm.begin(); // 初期化が必要です。

  SerialDebug.begin(115200); // デバッグ用
  UartModem.begin(115200);   // モデム用

  // 初期化が失敗した時のためリトライします
  int retry = 3;
  while (retry > 0)
  {
    if (modem_init(true))
      break;

    retry--;

    Dm.ModemPowerCtrl(OFF); // 電源切ってリトライします
    delay(100);
  }
  if (retry == 0)
  {
    SerialDebug.println("### MQTT Failed connection...");
    delay(100); // デバッグメッセージが出力終わるまで待ちます。
    Dm.reset(); // 再起動します
  }

  SerialDebug.println("### MQTT connected");
  _nextTimeout = millis(); // 送信周期を初期化します

  Dm.LedCtrl(GREEN_LED, OFF);
  Dm.LedCtrl(RED_LED, OFF);
}

void publish()
{
  JsonDocument doc;

  doc["counter"] = _count++;
  doc["gpio"] = String(Dm.readExIN());
  doc["adc"] = String(Dm.readExADC(), 3);

  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer);

  if (modem.mqttPublish(PUBLISH_TOPIC, jsonBuffer))
  {
    SerialDebug.println("published: " + String(jsonBuffer));
  }
  else
  {
    SerialDebug.println("publish failed...");
  }
}

void loop()
{
  // Subscribe処理
  int ret = modem.mqttLoop();
  if (ret > 0)
  {
    // subscribe受信
    char tmpBuf[32];
    DateTime now = Dm.rtcNow();
    snprintf(tmpBuf, sizeof(tmpBuf), "%04d/%02d/%02d %02d:%02d:%02d ", now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
    SerialDebug.println(tmpBuf);
    SerialDebug.println(modem.mqttMessage);
  }
  else if (ret < 0)
  {
    // 通信エラー発生、再接続等の処理が必要です
    modem_init(true);
  }

  // publish処理
  uint32_t timeStamp = millis();
  if ((timeStamp - _nextTimeout) >= SEND_CYCLE_MS)
  {
    _nextTimeout = timeStamp;
    Dm.LedCtrl(RED_LED, ON);
    publish();
    Dm.LedCtrl(RED_LED, OFF);
  }
}
